# Installation

## Prerequisites

 * Django >= 1.6 and its prerequisites http://docs.djangoproject.com/en/dev/intro/install/#intro-install
 * python-pbs >= 4.3 https://oss.trac.surfsara.nl/pbs_python

## Installation

Check out code from git:

    git clone https://github.com/VPAC/django-pbs.git django-pbs
    cd django-pbs
    python setup.py install

This will install django_pbs into something like /usr/lib/python2.7/site-packages/django_pbs

Debian packages can be built if required.

## Configuration

You will need to edit settings.py and change some variables, most options should be able to be left as default. You should only need to change the following

    DEBUG
    ADMINS
    LOCAL_PBS_SERVERS
    TIME_ZONE
    LANGUAGE_CODE

Sample Apache config (FIXME! should use mod-wsgi, not mod_python!)

    <Location "/djangopbs">
        SetHandler mod_python
        PythonHandler django.core.handlers.modpython
        PythonOption django.root /djangopbs
        SetEnv DJANGO_SETTINGS_MODULE django_pbs.settings
        PythonInterpreter djangopbs
        PythonDebug Off
    </Location>


    Alias /pbs_media "/path/to/django/pbs/media"

    <Location "/pbs_media">
        SetHandler None
    </Location>

    <Directory /path/to/django/pbs/media>
        AllowOverride None
        Order deny,allow
        Allow from all
    </Directory>
