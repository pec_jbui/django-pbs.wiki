# Django PBS

Django PBS is an application for viewing a pbs server from a web browser. You can see running jobs, nodes and queues.
It is license under the GPL version 3.

## Installation
See [[Installation]]

## Screenshot

![](https://raw.github.com/wiki/VPAC/django-pbs/Screenshot.png)
